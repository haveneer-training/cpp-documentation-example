# cpp-documentation-example
An example of setting up Sphinx for C++ and building with CMake and Read the Docs

See thes documentations
* [sphinx](https://haveneer-training.gitlab.io/cpp-documentation-example/sphinx)
* [doxygen](https://haveneer-training.gitlab.io/cpp-documentation-example/doxygen)

## Dependencies

- [CMake](https://cmake.org/download/)
- [Doxygen](http://www.doxygen.nl/download.html)
- [Sphinx](https://www.sphinx-doc.org/en/master/usage/installation.html)
- [Breathe](https://pypi.org/project/breathe/)
- [sphinx_rtd_theme](https://github.com/rtfd/sphinx_rtd_theme)

Thanks to TartanLlama for the [original example](https://github.com/TartanLlama/cpp-documentation-example).